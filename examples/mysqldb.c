#include "darknet.h"
#include <mysql.h>

MYSQL mysql, *sock;
int w_id = -1;
network *net = NULL;
int class_list[64] = {0};

void init_sql() {
  const char *host = "192.168.1.55";
  const char *user = "dl";
  // const char *host = "127.0.0.1";
  // const char *user = "root";
  const char *passwd = "123456";
  const char *db = "ai";
  unsigned int port = 3306;
  const char *unix_socket = NULL;
  unsigned long client_flag = 0;
  mysql_init(&mysql);
  if ((sock = mysql_real_connect(&mysql, host, user, passwd, db, port,
                                 unix_socket, client_flag)) == NULL) {
    printf("fail to connect mysql \n");
    fprintf(stderr, " %s\n", mysql_error(&mysql));
    exit(1);
  }
}

void close_sql() { mysql_close(sock); }

void save_results_to_db(int type, image im, int num, float thresh, detection *dets,
                        int classes, char *filename,
                        int weights_id, int model_id, int movie_id,
                        int frame_id) {
  int i, j;
  char sql[1024] = {0};
  // float confidence = 0.0;

  for (i = 0; i < num; ++i) {
    int class = -1;
    float confidence = 0.0;
    for(j = 0; j < classes; ++j){
        if (dets[i].prob[j] > thresh){
            if (class < 0) {
                //sprintf(labelstr, "%s_%.2f", names[j], dets[i].prob[j]);
                //strcat(labelstr, names[j]);
                class = j;
                confidence =dets[i].prob[class] ;
                printf("confidence = %f\n", confidence);
            }
          //  printf("%s: %.0f%%\n", names[j], probs[j]*100);
        }
    }
    if (class >= 0) {
      // int class_index = class;
      //float* confidence =dets[i].prob[class] ;
      //printf("confidence = %f", dets[i].prob[class]);
      box b = dets[i].bbox;
      int x1  = (b.x-b.w/2.)*im.w;
      int x2 = (b.x+b.w/2.)*im.w;
      int y1   = (b.y-b.h/2.)*im.h;
      int y2   = (b.y+b.h/2.)*im.h;

      if (x1 < 0)
        x1 = 0;
      if (x2 > im.w - 1)
        x2 = im.w - 1;
      if (y1 < 0)
        y1 = 0;
      if (y2 > im.h - 1)
        y2 = im.h - 1;

      memset(sql, 0x00, 1024);
      switch (type) {
        case 1:{
          sprintf(sql,
                  "insert into results "
                  "(weights_id, "
                  "model_id,movie_id,class_id,frame_id,class_index,confidence,"
                  "filename,x1,x2,y1,y2) values (%d,%d,%d,%d,%d,%d,%f,'%s',%d,%d,%d,%d)",
                  weights_id, model_id, movie_id, class_list[class], frame_id,
                  class, confidence, filename, x1, x2, y1, y2);
        }
        break;

        case 0:{
          sprintf(sql,
                  "insert into iou "
                  "(weights_id, "
                  "model_id,movie_id,class_id,frame_id,class_index,confidence,"
                  "filename,x1,x2,y1,y2) values (%d,%d,%d,%d,%d,%d,%f,'%s',%d,%d,%d,%d)",
                  weights_id, model_id, movie_id, class_list[class], frame_id,
                  class, confidence, filename, x1, x2, y1, y2);
        }
        break;


      }

      printf("%s\n", sql);
      mysql_query(&mysql, sql);
      mysql_affected_rows(&mysql);
      printf("save_results_to_db finished.\n");

    }
  }
}


void verify_frame(int type, network *net, char *fodler_name, char *filename,
                  int weights_id, int model_id, int movie_id, int frame_id) {
  // printf("verify_frame\n");
  // int j = 0;
  float nms = .3;
  char filepath[256] = {0};
  sprintf(filepath,
          "/ai_dl/%s/all_frame/%s",
          fodler_name, filename);
  image im = load_image_color(filepath, 0, 0);
  image sized = letterbox_image(im, net->w, net->h);
  layer l = net->layers[net->n - 1];
  float *X = sized.data;
  network_predict(net, X);
  int nboxes = 0;
  detection *dets = get_network_boxes(net, im.w, im.h, 0.01, 0.5, 0, 1, &nboxes);
  //printf("%d\n", nboxes);
  //if (nms) do_nms_obj(boxes, probs, l.w*l.h*l.n, l.classes, nms);
  if (nms) do_nms_sort(dets, nboxes, l.classes, nms);
  // printf("save_results_to_db\n");

  save_results_to_db(type, im, l.w * l.h * l.n, 0.01, dets, l.classes,
                     filename, weights_id, model_id, movie_id, frame_id);
  free_image(im);
  free_image(sized);
}

network * _load_weights(int weights_id) {

  if (w_id == weights_id) {
    return net;
  }

  printf("verify_from_db %d\n", weights_id);
  char sql[256] = {0};
  sprintf(sql, "select model_id, path from weights where id = %d", weights_id);
  mysql_query(&mysql, sql);
  MYSQL_RES *result = mysql_store_result(&mysql);
  MYSQL_ROW sqlrow = mysql_fetch_row(result);
  mysql_free_result(result);
  char cfgfile[128] = {0};
  sprintf(cfgfile, "/ai_dl/weights/%s/train.cfg", sqlrow[0]);
  char weightfile[128] = {0};
  sprintf(weightfile, "%s", sqlrow[1]);
  printf("weights: %s\n", weightfile);
  printf("config: %s\n", cfgfile);

  sprintf(sql,
          "select model_classes.class_id from weights "
"left join model_classes on model_classes.model_id = weights.model_id "
"left join classes on classes.id = model_classes.class_id "
"where weights.id = %d "
"order by classes.movie_id",
          weights_id);
  mysql_query(&mysql, sql);
  MYSQL_RES *result_classes = mysql_store_result(&mysql);
  printf("%s\n", sql);
  MYSQL_ROW sqlrow_classes;

  int cnt = 0;
  while ((sqlrow_classes = mysql_fetch_row(result_classes)) != NULL) {
    class_list[cnt++] = atoi(sqlrow_classes[0]);
    // printf("%s\n", sqlrow_classes[0]);
  }
  mysql_free_result(result_classes);

  if (net != NULL) {
    free_network(net);
  }

  net = load_network(cfgfile, weightfile, 0);
  // net = load_network("voc-tcl.cfg", "voc-tcl_40000.weights", 0);
  set_batch_network(net, 1);

  w_id = weights_id;

  return net;
}


void verify_from_db(int type, int offset) {
  init_sql();
  char sql[256] = {0};
  int flag = 1;
  printf("start \n");
  char fodler_name[128] = {};
  char filename[128] = {};
  while (flag == 1) {
    mysql_query(&mysql, "BEGIN;");
    sprintf(sql, "select * from verify_list limit %d,1;", offset);
    mysql_query(&mysql, sql);
    MYSQL_RES *result_row = mysql_store_result(&mysql);
    MYSQL_ROW verify_row = mysql_fetch_row(result_row);
    mysql_free_result(result_row);
    if (verify_row == NULL) {
      flag = 0;
    } else {
      int id = atoi(verify_row[0]);
      int weights_id = atoi(verify_row[1]);
      int model_id = atoi(verify_row[2]);
      int class_id = atoi(verify_row[3]);
      int movie_id = atoi(verify_row[4]);
      int frame_id = atoi(verify_row[5]);

      sprintf(fodler_name, "%s", verify_row[6]);
      sprintf(filename, "%s", verify_row[7]);
      // printf("id: %d\n", id);
      // printf("weights_id: %d\n", weights_id);
      // printf("model_id: %d\n", model_id);
      // printf("class_id: %d\n", class_id);
      // printf("movie_id: %d\n", movie_id);
      // printf("frame_id: %d\n", frame_id);
      // printf("fodler_name: %s\n", fodler_name);
      printf("filename: %s\n", filename);

      verify_frame(type, _load_weights(weights_id), fodler_name, filename, weights_id, model_id, movie_id,
                   frame_id);

      sprintf(sql, "delete from verify_list where id = %d;", id);
      // printf("%s\n", sql);
      mysql_query(&mysql, sql);
      mysql_query(&mysql, "COMMIT;");
    }
  }

  // verify_db(cfgfile, weightfile);
  // verify_db("voc-tcl.cfg", "voc-tcl_40000.weights");
  close_sql();
}
